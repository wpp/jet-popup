<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Popup_Generator' ) ) {

	/**
	 * Define Jet_Popup_Generator class
	 */
	class Jet_Popup_Generator {

		/**
		 * A reference to an instance of this class.
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private static $instance = null;

		/**
		 * [$popup_default_settings description]
		 * @var [type]
		 */
		public $popup_default_settings = [
			'jet_popup_type'                 => 'default',
			'jet_popup_animation'            => 'fade',
			'jet_popup_open_trigger'         => 'attach',
			'jet_popup_page_load_delay'      => 1,
			'jet_popup_user_inactivity_time' => 3,
			'jet_popup_scrolled_to_value'    => 10,
			'jet_popup_on_date_value'        => '',
			'jet_popup_custom_selector'      => '',
			'jet_popup_show_once'            => false,
			'jet_popup_show_again_delay'     => 'none',
			'jet_role_condition'             => [],
			'close_button_icon'              => 'fa fa-times',
		];

		/**
		 * Constructor for the class
		 */
		public function __construct() {

			// Page popup initialization
			add_action( 'wp_footer', array( $this, 'page_popup_init' ) );
		}

		/**
		 * Page popup initialization
		 *
		 * @since 1.0.0
		 * @return void|boolean
		 */
		public function page_popup_init() {

			if ( ! jet_popup()->has_elementor() || is_singular( jet_popup()->post_type->slug() ) || ! empty( $_GET['elementor-preview'] ) ) {
				return false;
			}

			$popup_id_list = [];

			$condition_popups = jet_popup()->conditions->find_matched_conditions( 'jet-popup' );

			if ( ! empty( $condition_popups ) && is_array( $condition_popups ) ) {
				$popup_id_list = array_merge( $popup_id_list, $condition_popups );
			}

			$attached_popups = jet_popup()->conditions->get_attached_popups();

			if ( ! empty( $attached_popups ) && is_array( $attached_popups ) ) {
				$popup_id_list = array_merge( $popup_id_list, $attached_popups );
			}

			if ( ! $popup_id_list || empty( $popup_id_list ) || ! is_array( $popup_id_list ) ) {
				return false;
			}

			$popup_id_list = array_unique( $popup_id_list );

			if ( ! empty( $popup_id_list ) ) {
				foreach ( $popup_id_list as $key => $popup_id ) {
					$this->popup_render( $popup_id );
				}
			}
		}

		/**
		 * [popup_render description]
		 * @param  [type] $popup_id [description]
		 * @return [type]           [description]
		 */
		public function popup_render( $popup_id ) {

			$meta_settings = get_post_meta( $popup_id, '_elementor_page_settings', true );

			$popup_settings_main = wp_parse_args( $meta_settings, $this->popup_default_settings );

			// Is Avaliable For User Check
			if ( ! $this->is_avaliable_for_user( $popup_settings_main['jet_role_condition'] ) ) {
				return false;
			}

			$close_button_html = '';

			$use_close_button = isset( $popup_settings_main['use_close_button'] ) ? filter_var( $popup_settings_main['use_close_button'], FILTER_VALIDATE_BOOLEAN ) : true;

			if ( isset( $popup_settings_main['close_button_icon'] ) && $use_close_button ) {
				$close_button_icon = $popup_settings_main['close_button_icon'];
				$close_button_html = sprintf( '<div class="jet-popup__close-button"><i class="%s"></i></div>', $close_button_icon );
			}

			$overlay_html = '';

			$use_overlay = isset( $popup_settings_main['use_overlay'] ) ? filter_var( $popup_settings_main['use_overlay'], FILTER_VALIDATE_BOOLEAN ) : true;

			if ( $use_overlay ) {
				$overlay_html = '<div class="jet-popup__overlay"></div>';
			}

			$jet_popup_show_again_delay = Jet_Popup_Utils::get_milliseconds_by_tag( $popup_settings_main['jet_popup_show_again_delay'] );

			$popup_json = [
				'id'                   => 'jet-popup-' . $popup_id,
				'type'                 => $popup_settings_main['jet_popup_type'],
				'animation'            => $popup_settings_main['jet_popup_animation'],
				'open-trigger'         => $popup_settings_main['jet_popup_open_trigger'],
				'page-load-delay'      => $popup_settings_main['jet_popup_page_load_delay'],
				'user-inactivity-time' => $popup_settings_main['jet_popup_user_inactivity_time'],
				'scrolled-to'          => $popup_settings_main['jet_popup_scrolled_to_value'],
				'on-date'              => $popup_settings_main['jet_popup_on_date_value'],
				'custom-selector'      => $popup_settings_main['jet_popup_custom_selector'],
				'show-once'            => filter_var( $popup_settings_main['jet_popup_show_once'], FILTER_VALIDATE_BOOLEAN ),
				'show-again-delay'     => $jet_popup_show_again_delay,
			];

			$popup_json_data = htmlspecialchars( json_encode( $popup_json ) );

			include jet_popup()->get_template( 'popup-container.php' );
		}

		/**
		 * [is_avaliable_for_user description]
		 * @param  [type]  $popup_roles [description]
		 * @return boolean              [description]
		 */
		public function is_avaliable_for_user( $popup_roles ) {

			if ( empty( $popup_roles ) ) {
				return true;
			}

			$user     = wp_get_current_user();
			$is_guest = empty( $user->roles ) ? true : false;

			if ( ! $is_guest ) {
				$user_role = $user->roles[0];
			} else {
				$user_role = 'guest';
			}

			if ( in_array( $user_role, $popup_roles ) ) {
				return true;
			}

			return false;
		}

		/**
		 * Output location content by template ID
		 * @param  integer $template_id [description]
		 * @param  string  $location    [description]
		 * @return [type]               [description]
		 */
		public function print_location_content( $template_id = 0 ) {

			$plugin    = Elementor\Plugin::instance();

			$content   = $plugin->frontend->get_builder_content( $template_id, false );

			if ( empty( $_GET['elementor-preview'] ) ) {
				echo $content;
			} else {
				printf(
					'<div class="jet-popup-edit">
						%1$s
						<a href="%2$s" target="_blank" class="jet-popup-edit__link elementor-clickable">
							<div class="jet-popup-edit__link-content"><span class="dashicons dashicons-edit"></span>%3$s</div>
						</a>
					</div>',
					$content,
					Elementor\Utils::get_edit_link( $template_id ),
					esc_html__( 'Edit Popup', 'jet-popup' )
				);
			}

		}

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @return object
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
	}

}
